<?php

namespace AppBundle\Entity;

class EventStorage implements \EventStorageInterface
{

    protected $event = [];

    public function store(\EventInterface $event)
    {
        $this->event[] = $event;
        return true;
    }
}