<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\EventStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use SSDMTechTest\EventProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    const SPORT = 'football';

    protected function getEventProcessor()
    {
        //ben lucas implementation
        return new EventProcessor(new EventStorage(), self::SPORT, $this->getAllowedEventTypes());


        //tech test three implementation - DOESNT WORK - REQUIRES CHANGE IN INTERFACE - WE DEFINED AS GLOBAL - TEST ASSUMES BUNDLED/NAMESPACES
//        return new \SSDMTechTest\Processor\Football\FootballProcessor(new EventStorage());

        //tech test two implementation
//        return new FootballEventProcessor(new EventStorage());

        //tech test four implementation
//        return new SportProcessor(new EventStorage(), self::SPORT, $this->getAllowedEventTypes());
    }


    /**
     * @Route("/expected_fail", name="fail")
     */
    public function failAction(Request $request)
    {

        $res = [];
        $events = $this->getBadEvents();
        $eventProcessor = $this->getEventProcessor();

        foreach ($events as $event) {

            $eventProcessRes = $eventProcessor->processEvent($event);
            if ($eventProcessRes != true) {
                $res[] = $event->getSport() . " " . $event->getEventType() . " Failed - Well Done";
            } else {
                throw new Exception('Event Should Not Have Been Allowed', 500);

            }
        }

        if (empty($res)) {
            throw new Exception('Event Should Not Have Been Allowed', 500);
        }
        return $this->getView($res);
    }


    protected function getView($response)
    {
        // replace this example code with whatever you need
        return $this->render('test.info.html.twig',['body' => $response]);
    }
    /**
     * @Route("/expected_pass", name="pass")
     */
    public function passAction(Request $request)
    {
        $events = $this->getEvents();
        $eventProcessor = $this->getEventProcessor();
        foreach ($events as $event) {

            $res = $eventProcessor->processEvent($event);
            if ($res != true) {
                throw new Exception('Event Not Allowed: ' . $event->getSport() . " " . $event->getEventType(), 500);
            }
        }
        return $this->getView(["Passed"]);

    }

    /**
     * @return array
     */
    protected function getAllowedEventTypes()
    {
        return $allowedEventTypes = [
            "kickoff",
            "goal",
            "yellowcard",
            "redcard",
            "penalty",
            "halftime",
            "fulltime",
            "extratime",
            "freekick",
            "corner"
        ];
    }

    protected function getEvents()
    {
        $events = [];
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('kickoff');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('goal');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('yellowcard');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('redcard');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('penalty');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('halftime');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('kickoff');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('fulltime');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('extratime');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('freekick');
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('corner');
        return $events;
    }

    protected function getBadEvents()
    {
        $events = [];
        $events[] = (new Event())->setSport(self::SPORT)->setEventType('afterextratime');
        $events[] = (new Event())->setSport('RugbyUnion')->setEventType('kickoff');
        return $events;
    }
}
