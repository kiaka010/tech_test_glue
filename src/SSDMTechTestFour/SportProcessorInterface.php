<?php
namespace SSDMTechTest;

use EventInterface;

/**
 * @author Daniel Chesterton <daniel@chestertondevelopment.com>
 */
interface SportProcessorInterface
{
    public function processEvent(EventInterface $event);
}
