<?php
namespace SSDMTechTest;

use EventStorageInterface;

/**
 * Processes football events.
 *
 * @author Daniel Chesterton <daniel@chestertondevelopment.com>
 */
class FootballProcessor extends SportProcessor
{
	/**
	 * @param EventStorageInterface $storage
	 */
	public function __construct(EventStorageInterface $storage)
	{
		parent::__construct($storage, 'football', [
			'kickoff',
			'goal',
			'yellowcard',
			'redcard',
			'penalty',
			'halftime',
			'fulltime',
			'extratime',
			'freekick',
			'corner'
		]);
	}
}
