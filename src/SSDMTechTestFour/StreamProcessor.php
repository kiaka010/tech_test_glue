<?php
namespace SSDMTechTest;

/**
 * Processes an incoming data stream. Provide a list of processors, one for each sport, which
 * must implement {@see SportProcessorInterface}.
 *
 * @author Daniel Chesterton <daniel@chestertondevelopment.com>
 */
class StreamProcessor
{
    /**
     * @var SportProcessorInterface[]
     */
    private $processors;

    /**
     * @param array $processors An array of processors, one for each sport
     * @throws \Exception
     */
    public function __construct(array $processors)
    {
        foreach ($processors as $processor) {
            if (!$processor instanceof SportProcessorInterface) {
                throw new \InvalidArgumentException('Processors must implement SportProcessorInterface');
            }
        }

        $this->processors = $processors;
    }

    /**
     * @param \EventInterface $event
     *
     * @return bool
     */
    public function processEvent(\EventInterface $event)
    {
        foreach ($this->processors as $processor) {
            if ($processor->processEvent($event)) {
                return true;
            }
        }

        return false;
    }
}
