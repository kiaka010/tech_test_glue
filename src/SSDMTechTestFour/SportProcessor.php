<?php
namespace SSDMTechTest;

use EventInterface;
use EventStorageInterface;

/**
 * Processes events for a given sport and event types.
 *
 * @author Daniel Chesterton <daniel@chestertondevelopment.com>
 */
class SportProcessor implements SportProcessorInterface
{
	/**
	 * @var EventStorageInterface
	 */
	private $storage;

	/**
	 * @var string
	 */
	private $sport;

	/**
	 * @var array
	 */
	private $eventTypes;

	/**
	 * @param EventStorageInterface $storage
	 * @param string                $sport       The sport to process events for
	 * @param array                 $eventTypes  An array of event types (strings) to process
	 */
	public function __construct(EventStorageInterface $storage, $sport, array $eventTypes)
	{
		$this->storage = $storage;
		$this->sport = $sport;
		$this->eventTypes = $eventTypes;
	}

    /**
     * @param EventInterface $event
     *
     * @return bool Whether the event was processed or not
     */
	public function processEvent(EventInterface $event)
	{
		if ($event->getSport() !== $this->sport) {
			return false;
		}

		if (!in_array($event->getEventType(), $this->eventTypes, true)) {
			return false;
		}

		$this->storage->store($event);

        return true;
	}
}
