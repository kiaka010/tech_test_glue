<?php

namespace SSDMTechTest;

use EventInterface;
use EventStorageInterface;

class EventProcessor {
    /** @var EventStorageInterface */
    private $storage;

    /** @var string */
    private $sport;

    /** @var array */
    private $eventTypes;

    /**
     * @param EventStorageInterface $storage
     * @param string $sport
     * @param array $eventTypes
     */
    public function __construct(
        EventStorageInterface $storage,
        $sport,
        array $eventTypes
    ) {
        $this->storage = $storage;
        $this->sport = $sport;
        $this->eventTypes = $eventTypes;
    }

    /**
     * @param EventInterface $event
     * @return bool
     */
    private function isValidEvent(EventInterface $event)
    {
        if ($event->getSport() !== $this->sport) {
            return false;
        }

        return in_array($event->getEventType(), $this->eventTypes);
    }

    /**
     * Stores a valid event.
     * @param EventInterface $event
     * @return bool
     */
    public function processEvent(EventInterface $event)
    {
        if ($this->isValidEvent($event)){
            return $this->storage->store($event);
        }

        return false;
    }
}