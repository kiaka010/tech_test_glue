<?php

namespace SSDMTechTest\Processor\Football;

/**
 * Class FootballEvents
 * @package SSDMTechTest
 */
final class FootballEvents
{
    /**
     * @var string
     */
    const EVENT_KICKOFF = 'kickoff';

    /**
     * @var string
     */
    const EVENT_GOAL = 'goal';

    /**
     * @var string
     */
    const EVENT_YELLOWCARD = 'yellowcard';

    /**
     * @var string
     */
    const EVENT_REDCARD = 'redcard';

    /**
     * @var string
     */
    const EVENT_PENALTY = 'penalty';

    /**
     * @var string
     */
    const EVENT_HALFTIME = 'halftime';

    /**
     * @var string
     */
    const EVENT_FULLTIME = 'fulltime';

    /**
     * @var string
     */
    const EVENT_EXTRATIME = 'extratime';

    /**
     * @var string
     */
    const EVENT_FREEKICK = 'freekick';

    /**
     * @var string
     */
    const EVENT_CORNER = 'corner';
}