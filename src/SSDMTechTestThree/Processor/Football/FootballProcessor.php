<?php

namespace SSDMTechTest\Processor\Football;

use SSDMTechTest\Processor\AbstractProcessor;
use SSDMTechTest\EventInterface;

/**
 * Class FootballProcessor
 * @package SSDMTechTest
 */
class FootballProcessor extends AbstractProcessor
{
    /**
     * @inheritdoc
     */
    protected static $sport = 'football';

    /**
     * @inheritdoc
     */
    protected static $eventTypes = 'SSDMTechTest\\Processor\\Football\\FootballEvents';

    /**
     * @param EventInterface $event
     */
    protected function protectedProcess(EventInterface $event)
    {
        // Add custom handling
    }
}