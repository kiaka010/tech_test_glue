<?php

namespace SSDMTechTest\Processor;

use ReflectionClass;
use SSDMTechTest\EventInterface;
use SSDMTechTest\EventStorageInterface;

/**
 * Class AbstractProcessor
 * @package SSDMTechTest
 */
abstract class AbstractProcessor
{
    /**
     * @var string The name of the sport
     */
    protected static $sport;

    /**
     * @var string The namespace of a class with the event types of the sport as constants
     */
    protected static $eventTypes;

    /**
     * @var EventStorageInterface
     */
    protected $eventStorage;

    /**
     * @param EventStorageInterface $eventStorage
     */
    public function __construct(EventStorageInterface $eventStorage)
    {
        $this->eventStorage = $eventStorage;
    }

    /**
     * @param EventInterface $event
     * @return boolean
     */
    public function isValidSport(EventInterface $event)
    {
        return static::$sport==$event->getSport();
    }

    /**
     * @param EventInterface $event
     * @return boolean
     */
    public function isValidType(EventInterface $event)
    {
        $eventTypes = new ReflectionClass(static::$eventTypes);

        return in_array($event->getEventType(), $eventTypes->getConstants());
    }

    /**
     * @param EventInterface $event
     */
    public function process(EventInterface $event)
    {
        if ($this->isValidSport($event) && $this->isValidType($event)) {
            $this->protectedProcess($event);
            $this->eventStorage->store($event);
        }
    }

    /**
     * @param EventInterface $event
     */
    abstract protected function protectedProcess(EventInterface $event);
}
