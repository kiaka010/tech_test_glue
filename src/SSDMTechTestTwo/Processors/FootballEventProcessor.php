<?php

namespace SSDMTechTest\Processors;

use EventInterface;

/**
 * Class FootballEventProcessor
 */
class FootballEventProcessor extends EventProcessor
{
    /**
     * The name of the event supported
     *
     * @var string
     */
    protected $sport = 'football';

    /**
     * An array of event types supported
     *
     * @var array
     */
    protected $eventTypes = [
        'kickoff',
        'goal',
        'yellowcard',
        'redcard',
        'penalty',
        'halftime',
        'fulltime',
        'extratime',
        'freekick',
        'corner',
    ];

    /**
     * {@inheritdoc}
     */
    public function processEvent(EventInterface $event)
    {
        if ($this->supports($event)) {
            //$footballGame = new \FootballGame('Leeds United', 'Manchester United', '2017-02-10 20:00:00');
            //$event->setFootballGame($footballGame);

            // The event has been populated, now store it!
            $this->eventStorage->store($event);

            return true;
        }

        return false;
    }
}
