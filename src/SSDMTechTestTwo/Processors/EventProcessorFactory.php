<?php

namespace SSDMTechTest\Processors;

use EventInterface;

/**
 * Class EventProcessorFactory
 */
class EventProcessorFactory
{
    /**
     * An array of available processors
     *
     * @var array
     */
    private $eventProcessors = [];

    /**
     * Adds a new processor to the factory
     *
     * @param EventProcessor[] $eventProcessors
     */
    public function addProcessors(array $eventProcessors)
    {
        foreach ($eventProcessors as $eventProcessor) {
            if (!$eventProcessor instanceof EventProcessor) {
                continue;
            }

            $this->eventProcessors[] = $eventProcessor;
        }
    }

    /**
     * Returns the array of supported event processors
     *
     * @return array
     */
    public function getEventProcessors()
    {
        return $this->eventProcessors;
    }

    /**
     * Returns the event processor for the event
     *
     * @param EventInterface $event
     *
     * @return EventProcessor
     *
     * @throws \Exception If a processor cannot be found for the event provided
     */
    public function getProcessor(EventInterface $event)
    {
        /** @var EventProcessor $eventProcessor */
        foreach ($this->eventProcessors as $eventProcessor) {
            if ($eventProcessor->supports($event)) {
                return $eventProcessor;
            }
        }

        throw new \Exception('Event Processor not found for event.');
    }
}
