<?php

namespace SSDMTechTest\Processors;

use EventInterface;
use EventStorageInterface;

/**
 * Class EventProcessor
 */
abstract class EventProcessor
{
    /**
     * The supported event
     *
     * @var string
     */
    protected $sport;

    /**
     * An array of supported event types
     *
     * @var array
     */
    protected $eventTypes = [];

    /**
     * The event storage class
     *
     * @var EventStorageInterface
     */
    protected $eventStorage;

    /**
     * EventProcessor constructor.
     *
     * @param EventStorageInterface $eventStorage
     */
    public function __construct(EventStorageInterface $eventStorage)
    {
        $this->eventStorage = $eventStorage;
    }

    /**
     * Processes an event and returns a boolean
     *
     * @param EventInterface $event
     *
     * @return boolean
     */
    abstract public function processEvent(EventInterface $event);

    /**
     * Determines whether this processor supports the event provided
     *
     * @param EventInterface $event
     *
     * @return boolean
     */
    public function supports(EventInterface $event)
    {
        return $event->getSport() === $this->sport &&
            in_array($event->getEventType(), $this->eventTypes);
    }

    /**
     * Returns the sport supported by this processor
     *
     * @return string
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Returns an array of supported event types
     *
     * @return array
     */
    public function getEventTypes()
    {
        return $this->eventTypes;
    }
}
