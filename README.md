tech_test_controller
====================

A Symfony project created on February 27, 2017, 3:14 pm.

##How to use
Copy the SRC folder from the tech test into the srs/SSDMTechTest bundle
Copy the TESTS folder from the tech test into the tests/SSDMTechTest bundle

updates to /src/AppBundle/Controller/DefaultController.php will need to be done access the processor of the tech test - other tech tests can be added as spertate bundles for reference
 
- run `php bin/console server:run`
- goto <http://127.0.0.1:8000/expected_pass> and see info on expected passes
- goto <http://127.0.0.1:8000/expected_fail> and see info on expected fails
- run `phpunit` to see all tests pass